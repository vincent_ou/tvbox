
TVBox数据接口TVBox数据配置源汇总【最新更新于2025-3-5】

TVbox 本身是一款空壳子APP，类似于功能性的播放器，你填进去什么它就会播放什么，所以“种瓜得瓜，种豆得豆”，里面的内容完全取之视频源的爬取。

🥰大家要搞清楚“单线路”、“多线路”、“多仓”概念，线路，一次推送一个线路；多线路，几个或几十个线路组成多线路，多个多线路组成多仓.

推送分清用“单线路”、“多线路”、“多仓”软件比如：

多仓软件：影视仓、宝盒。。。

多线软件：FM壳子、OK壳子、影视仓、宝盒。。。

单线软件：TVBOx、FM壳子、OK壳子、影视仓、宝盒。。。

接口地址在最下面点复制推送到仓库或者粘到配置就OK

温馨提示：接口完全免费，切勿付费购买。请勿相信视频中任何广告
TVBox数据源接口：

目前可用线路（已验证）

肥猫接口：http://ceshi.肥猫.com/PandaQ

肥猫备用接口：http://肥猫.com

饭太硬接口：http://www.饭太硬.com/tv

饭太硬（备用）：http://fty.xxooo.cf/tv

饭太硬（备用）：http://fty.888484.xyz/tv

毒盒：https://毒盒.com/tv

小盒子4K：http://xhztv.top/4k.json

欧歌4K：http://tv.nxog.top/m

欧歌免费：https://tv.nxog.top/m/111.php?ou=公众号欧歌app&mz=index&jar=index&123&b=tv

蓝天Luck（央视大全）：https://gitee.com/lukei7/lib/raw/Luck/%E8%87%AA%E5%BB%BA.json

小米接口：http://www.mpanso.com/小米/DEMO.json

王二小接口：http://tvbox.xn--4kq62z5rby2qupq9ub.top/

公众号【王二小放牛娃】：http://tv.999888987.xyz

南风接口：https://raw.githubusercontent.com/yoursmile66/TVBox/main/XC.json

南风接口（代理）：https://gh.aptv.app/https://raw.githubusercontent.com/yoursmile66/TVBox/main/XC.json

OK吊炸天接口（使用【蜂蜜即FongMi】，其它空壳需要激活码）：http://ok321.top/tv

https://yydf.540734621.xyz/QQ/yydf2024.json

巧技接口：http://cdn.qiaoji8.com/tvbox.json

吃猫的鱼接口：https://d.kstore.dev/download/7213/吃猫的鱼

欧歌单线路接口：https://xn--xkkx-rp5imh.v.nxog.top/api.php?id=3

菜妮丝XBPQ接口：https://tv.xn--yhqu5zs87a.top

摸鱼儿：http://我不是.摸鱼儿.com

4K影视仓：http://4K4K.shop/

天微科技：https://github.moeyy.xyz/https://raw.githubusercontent.com/vcloudc/tvbox/main/tw/api.json

公众号【非凡小小】：https://g.3344550.xyz/https://raw.githubusercontent.com/jigedos/1024/master/jsm.json

二月红接口：https://700sjro44343.vicp.fun/eggp/0211/tv.json

公众号【挺好分享】：http://ztha.top/TVBox/thdjk.json

公众号【蓝天日记】：https://gitee.com/lukei7/lib/raw/Luck/%E8%87%AA%E5%BB%BA.json

传说blog：https://chuanshuo.77blog.cn/tv.json

青龙：https://gitee.com/yiwu369/6758/raw/master/%E9%9D%92%E9%BE%99/1.json

香雅情短剧：http://74.120.175.78/JK/XYQTVBox/dj.json

短剧频道：http://box.ufuzi.com/tv/qq/短剧频道/api.json

少儿频道：https://jihulab.com/ymz1231/xymz/-/raw/main/ymshaoer

欧歌接口：https://xn--sdds-rp5imh.v.nxog.top/apitv.php?id=3

俊哥接口：http://home.jundie.top:81/top98.json

4K云盘接口：https://9xi4o.tk/0725.json

巧儿接口：http://pandown.pro/tvbox/tvbox.json

胖虎接口：https://notabug.org/imbig66/tv-spider-man/raw/master/%E9%85%8D%E7%BD%AE/0801.json

巧技:http://pandown.pro/tvbox/tvbox.json

黄金分割工作室：https://gitlab.com/lzc1021lzc/hjfggzs.hjys/-/raw/main/hjys.free.json

ocean2025：https://git.acwing.com/ocean2025/ocean/-/raw/main/api.json

其他大佬接口：

驸马影视：http://fmys.top/fmys.json

http://www.fish2018.us.kg/p/jsm.json

https://git.acwing.com/iduoduo/orange/-/raw/main/config.bin

https://ghproxy.net/https://raw.githubusercontent.com/xiaolinshao/linshao/main/1.json

https://www.lintech.work/%E8%B6%85%E5%A4%A7%E6%9D%AF/main.json

http://124.71.189.194/a.json

http://meowtv.top/tv

https://kjsc0310.github.io/tvy/jk9.json

https://dxawi.github.io/0/0.json

http://home.jundie.top:81/TVBox/yosakoi.json

https://raw.liucn.cc/box/m.json

https://wds.ecsxs.com/212757.json

https://liu673cn.github.io/box/m.json

https://pastebin.com/raw/gtbKvnE1

https://cdn.jsdelivr.net/gh/GaiVmao/dianshiyuan@main/yuan2.txt

http://pandown.pro/tvbox/tvbox.json

https://dxawi.github.io/0/0.json

单仓地址：

小盒子单仓：http://xhztv.top/xhz

★潇洒单仓：https://9877.kstore.space/FourDS/api.json

★开心单仓：http://kxrj.site:55

★星辰单仓：http://47.99.102.252/dc.json

★欧歌多线路：https://xn--occo-rp5imh.v.nxog.top/api.php?id=2

多仓地址（适用于:影视仓.宝盒·蜂蜜/Ok影视）：

无邪多仓：https://gitee.com/wxej/wxrj/raw/master/wx.json

小盒子多仓：http://xhztv.top/tvbox.txt

蜗牛科技仓：https://tv.蜗牛.top/DS

天天开心多仓：http://rihou.cc:55

念心多仓：https://pz.nianxin.top/nxD.json

拾光多仓：https://qixing.myhkw.com/DC.txt

玩盒助手多仓：https://chuanshuo.77blog.cn/dc.json

玩盒助手多仓备用：https://jihulab.com/chuanshuo/box/raw/main/duo

神州多仓：http://m6z.cn/6pUcwV

多多多仓：https://bitbucket.org/xduo/cool/raw/main/room.json

https://12586.kstore.space/123.json

★欧歌多仓接口：https://xn--lhhl-rp5imh.v.nxog.top/api.php?id=1

欧歌多仓：http://m.nxog.top/nxog/ou1.php?url=http://tv.nxog.top&b=欧歌

下面是收集了大量的TVBox接口，通过导入特定资源接口，解析各类爬虫源、XP源、采集源等。完全无任何限制，也无任何广告。

源码

https://github.com/liu673cn/box/raw/main/m.json

香港

https://raw.iqiq.io/liu673cn/box/main/m.json

新加坡

https://raw.kgithub.com/liu673cn/box/main/m.json

日本

https://fastly.jsdelivr.net/gh/liu673cn/box@main/m.json
https://cdn.staticaly.com/gh/liu673cn/box/main/m.json
https://raw.fastgit.org/liu673cn/box/main/m.json

韩国

https://ghproxy.com/https://raw.githubusercontent.com/liu673cn/box/main/m.json
https://ghproxy.net/https://raw.githubusercontent.com/liu673cn/box/main/m.json
https://gcore.jsdelivr.net/gh/liu673cn/box@main/m.json
https://raw.githubusercontents.com/liu673cn/box/main/m.json

Github 静态加速

https://cdn.staticaly.com/gh/liu673cn/box/main/m.json
https://cdn.jsdelivr.net/gh/liu673cn/box@main/m.json
https://purge.jsdelivr.net/gh/
TVBox直播源接口：

直播文件下载地址:

https://gofile.io/d/RQb56X

直播源搜索：

https://www.foodieguide.com/iptvsearch/?s=

http://tonkiang.us/

直播接口

https://agit.ai/945KM/TVBox/raw/branch/master/TV/live.txt
已失效接口

 

天天开心接口：http://kxrj.site:55/天天开心

潇洒接口：https://9877.kstore.space/FourD/api.json

纯一骚零接口：https://100km.top/0

小米备用接口：https://mi.mpanso.me/DEMO.json

放牛娃：http://tvbox.xn--4kq62z5rby2qupq9ub.xyz/

饭太硬：http://饭太硬.ga/tv

肥猫1：http://我不是.肥猫.love:63/接口禁止贩卖

肥猫2：http://肥猫.live

高天流云js：https://ghproxy.com/raw.githubusercontent.com/gaotianliuyun/gao/master/js.json

菜妮丝XBPQ:https://tvbox.cainisi.cf

霜辉月明py:https://ghproxy.com/raw.githubusercontent.com/lm317379829/PyramidStore/pyramid/py.json

FongMl:https://ghproxy.com/raw.githubusercontent.com/FongMi/CatVodSpider/main/json/config.json

唐三:https://hutool.ml/tang

云星日记：https://ghproxy.com/raw.githubusercontent.com/FongMi/CatVodSpider/main/json/config.json

945KM接口：https://agit.ai/945KM/TVBox/raw/branch/master/1.json

唐三接口：https://hutool.ml/tang

肥猫接口：http://肥猫.love

莱妮丝：https://tvbox.cainisi.cf

分享君接口：http://byyds.top/w.txt

南风接口：https://agit.ai/Yoursmile7/TVBox/raw/branch/master/XC.json

4K云盘接口：http://9xi4o.tk/0725.json

老刘备接口：https://raw.iqiq.io/liu673cn/box/main/m.json

白嫖线路：http://js.134584.xyz/json/pp87.json

乱世接口：http://www.dmtv.ml/mao/single.json

太阳线路：http://111.67.196.181/mtv/meow.txt

小苹果接口（内选择节目里有广告，注意）：https://agit.ai/nbwzlyd/xiaopingguo/raw/branch/master/xiaopingguo/xiaopingguo.json

http://byyds.top/w.txt

https://www.cai512.eu.org/a.json


https://gitea.com/Yoursmile/TVBox/raw/branch/main/XC.json

https://gitea.com/nb/kls/raw/branch/master/a9.json

https://gitea.com/hfr1107/TVBox/raw/branch/main/sc.json

https://pastebin.com/raw/sbPpDm9G

https://wds.ecsxs.com/223843.txt

https://cn.kstore.space/download/2710/zn777/kaisu220821.txt

https://out.zxglife.top/down.php/5da920895444ba485dde96b5425b8a67.txt

https://try.gitea.io/zn777/yshz/raw/branch/main/gitea824.txt

https://pan.tenire.com/down.php/2664dabf44e1b55919f481903a178cba.txt

https://apis.fanyeyun.com/down.php/2664dabf44e1b55919f481903a178cba.txt

http://wp.anxwl.cn/down.php/5219c327b90a65fc2020c77ba6dc6911.txt

http://s.nxw.so/vip

http://52bsj.vip:81/api/v3/file/get/29899/box2.json?sign=3cVyKZQr3lFAwdB3HK-A7h33e0MnmG6lLB9oWlvSNnM%3D%3A0

https://ghproxy.com/https://raw.githubusercontent.com/chengxueli818913/maoTV/main/44.txt

https://download.kstore.space/download/2863/01.txt

https://agit.ai/n/b/raw/branch/a/b/c.json

https://freed.yuanhsing.cf/TVBox/meowcf.json

https://cdn.staticaly.com/gh/XuQqu/PinkBird/main/normal.json

https://ju.binghe.ga/4.txt

http://xn--4bra.live/gg.json

https://box.okeybox.top/tv/shyi.json

https://box.okeybox.top/tv/vyo2.json

https://box.okeybox.top/tv/5gup.json

https://box.okeybox.top/tv/fj0p.json

https://box.okeybox.top/tv/2wti.json

https://box.okeybox.top/tv/64u1.json

https://box.okeybox.top/tv/o5oh.json

https://box.okeybox.top/tv/iw3y.json

https://box.okeybox.top/tv/sx39.json

https://box.okeybox.top/tv/91qn.json

https://gitea.com/xyyx/TVBox/raw/branch/main/tv.json

https://gitea.com/kanwidgg/kankan/raw/branch/main/0821XC/XC.json

https://gitea.com/jiaojiao/jiaojiao/raw/branch/main/0821.json

https://gitea.com/kanwidgg/kankan/raw/branch/main/002-0821/gg.json

https://gitea.com/playgayme/boxRepository/raw/branch/main/TVBox/xzy.json

https://gitea.com/yuegu09/aziy/raw/branch/main/zi71501.json

https://gitea.com/hongdeng98/YH/raw/branch/main/Mao0820.json

https://gitee.com/wyy007cn/tv/raw/master/tv1.json

https://c1n.cn/2

https://maoys.c1n.cn

https://pastebin.com/raw/FuMsfJWw

https://tool.wotianna.com/mysjk.json

https://wds.ecsxs.com/213821.json

https://wds.ecsxs.com/213679.json

https://c1n.cn/cat

http://gg.gg/cccvvv

https://wds.ecsxs.com/213013.json

https://c1n.cn/mao

https://wds.ecsxs.com/216537.json

https://wds.ecsxs.com/213766.txt

https://wds.ecsxs.com/213313.json

http://xn--4bra.live/%E7%8C%AB

http://js.134584.xyz/json/pp87.json

http://52bsj.vip:98/wuai

https://gitee.com/jjjsndjdjjjjd/tv-storage/raw/master/tv.txt

https://wds.ecsxs.com/223843.txt

https://gitcode.net/qq_26898231/TVBox/-/raw/main/tv.json

https://cdn.jsdelivr.net/gh/chengxueli818913/maoTV@main/44.txt

http://miaotvs.cn/osc

https://raw.iqiq.io/liu673cn/box/main/m.json

http://shuyuan.miaogongzi.net/shuyuan/1663651243.txt

https://raw.iqiq.io/zhanghong1983/TVBOXZY/main/TVBOX/iqiqgr.json

https://try.gitea.io/xcxc8/mytv/raw/branch/main/TV.json

https://神器每日推送.tk/pz.json

http://饭太硬.ga/x/o.json

https://源享家.ml/tv

http://drpy.site/js1

http://js.134584.xyz/json/pp87.json

http://byyds.top/w.txt

https://agit.ai/xiaohewanwan/jar/raw/branch/main/Avatar.json
